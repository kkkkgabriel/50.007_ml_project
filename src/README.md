Instructions to how run code:

1. Open jupyter notebook sentimentAnalysis_v1.ipynb
2. Change the foldername in cell set key Vars according to which dataset you want to create predictions for ("EN" || "CN" || "SG")
3. Run the rest of the notebook