DEFAULTDATAFOLDER = 'EN'
DEFAULTDATANAME = 'train'
DEFAULTDATAPATH = 'data/raw/'

import pandas as pd

class C:
	# highlights
	REDH 	 	= '\u001b[41;1m'
	GREENH   	= '\u001b[42;1m'
	YELLOWH  	= '\u001b[43;1m'
	BLUEH 	 	= '\u001b[44;1m'
	PURPLEH 	= '\u001b[45;1m'
	CYANH 		= '\u001b[46;1m'

	# colors 
	RED			= '\033[91;1m'
	GREEN		= '\033[92;1m'
	YELLOW 		= '\033[93;1m'
	BLUE 		= '\033[94;1m'
	PURPLE 		= '\033[95;1m'
	CYAN 		= '\033[96;1m'

	# misc
	ENDC 		= '\033[0m'
	BOLD 		= '\033[1m'
	UNDERLINE 	= '\033[4m'

	def info():
		print('RED, GREEN, YELLOW, BLUE, PURPLE, CYAN, + "H"')

def color(word, color):
	return color + word + C.ENDC

def readRawConll(path, format='TRAIN'):
	'''
	Reads the data and returns the data in a custom format
	params:
		path (str) => location of data to be read
		format 'TRAIN' || 'TEST' (str) => , if format is set to train, labels are expected.
	return: 
		data (list) => If format is set to 'TRAIN', data will be list of tuples of (text, fullText).
			If format is set to 'TEST', data will be a list of tuples of (text, labels, fullText)
	'''
	try:
		# read the file
		f = open(path, 'r', encoding='utf8')
		lines = f.readlines()
		f.close()

		# init vars for loop
		data = []
		text, label = [], []

		# loop through lines
		for line in lines:
			# if line is not empty, continue adding to list of text and labels
			if not line == '\n':
				textAndLabel = line.split()

				text.append(textAndLabel[0])

				# only look for labels if format is set to train (only training data has labels)
				if format == 'TRAIN':
					label.append(textAndLabel[1])

			# if line is empty, this means the previous sentence has ended.
			# add text and label to data, and re-init text and label
			else:
				fullText = ' '.join(text)

				# only append labels if format is set to train (only training data has label)
				if format == 'TRAIN':
					data.append((text, label, fullText))
				else:
					data.append((text, fullText))

				text, label = [], []

		return data

	except Exception as e:
		print('Error while reading file:')
		print(e)
		return


def exportPredictionsToConll(textList, predictedLabelsList, exportLoc):
	'''
	Exports prediction to conll format for evaluation
	params: 
		textList (list) => nested list of all the words
		predictedLabelsList (list) => nested list of all the predicted labels 
		exportLoc (str) => location to save the file
	'''
	# open file and set file to write mode
	f = open(exportLoc, 'w', encoding='utf8')

	# loop through each list of text and corresponding list labels
	for text, labels in zip(textList, predictedLabelsList):
		
		# loop through each word and correspondign label
		for word, label in zip(text, labels):
			# write the prediction and add a breakline
			f.write(word + ' ' + label)
			f.write('\n')
			
		# add a breakline after every text
		f.write('\n')
	f.close()
