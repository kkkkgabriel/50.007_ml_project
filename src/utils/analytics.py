from tqdm import tqdm
import numpy as np

def computeStatesAndTransitions(textList, labelList):
	'''
	loops through the data to compute the states frequency and transition frequency,
	and return the data as dictionaries

	params:
		textList (list) => list of texts to be processed
		LabelList (list) => nested list of labels corresponding to each word in each text
	returns:
		labels (dic) => dic containing {'label': (frequency, wordlist), ...}
		labelTransitions (dic) => dic containing {'labelTransition': (frequency, wordlist), ...}
		wordTransitions (dic) => dic containing {'stateTransition': (frequency, labelTransition), ...}
		labelWordTransitions (dic) => dic containing {'labelWordTransitioin': frequency, ...}
	'''
	# init vars
	labelWordList = {}       # to store the word list coresponding to each label
	labelTransitions = {}    # to store the word list of each label transition
	wordTransitions = {}     # to store the word transitions corresponding to each type of state transition
	labelWordTransitions = {} # to store the label word transition

	# loop through each row
	for labels, text in zip(labelList, textList):

		# init vars for looping
		prevLabel = None
		prevWord = None

		# split text into list and add start and stop tokens
		labels = ['START'] + labels + ['STOP']
		if isinstance(text, str):
			text = ['START'] + text.split() + ['STOP']
		else:
			text = ['START'] + text + ['STOP']

		# loop through each text and label
		for word, label in zip(text, labels):


			if label != 'START' and label != 'STOP':
				#-- insert label into label freq --#
				# add word to labelWordList[label]
				if label in labelWordList:
					labelWordList[label].append(word)
				else:
					labelWordList[label] = [word]

				#-- create label word transition --#
				# create label word transition strings
				labelWordTransition = label + ' > ' + str(word)
				# add 1 to count, or start count at 1
				if labelWordTransition in labelWordTransitions:
					labelWordTransitions[labelWordTransition] += 1
				else:
					labelWordTransitions[labelWordTransition] = 1

			if prevLabel and str(prevWord):
				# create transition strings
				labelTransition = prevLabel + ' > ' + label
				wordTransition = str(prevWord) + ' > ' + str(word)

				#-- insert label into label transitions --#
				# add word transition into labelTransitions[labelTransition]
				if labelTransition in labelTransitions:
					labelTransitions[labelTransition].append(wordTransition)
				else:
					labelTransitions[labelTransition] = [wordTransition]
					
				#-- insert word into word transitions --#
				# add labelTransition into wordTransitions[labelTransition]
				if wordTransition in wordTransitions:
					wordTransitions[wordTransition].append(labelTransition)
				else:
					wordTransitions[wordTransition] = [labelTransition]

			# set prevState as current label for next iteration
			prevLabel = label
			prevWord = word

	# add in empty transitions as well
	labels = list(labelWordList.keys())
	for label1 in labels:
		for label2 in labels:
			labelTransition = label1 + ' > ' + label2
			# add label transition into freq if not yet added
			if not labelTransition in labelTransitions:
				labelTransitions[labelTransition] = []

	# create dictionaries
	labels = {l: (f, w) for l, f, w in zip(labelWordList.keys(), [len(words) for words in labelWordList.values()], labelWordList.values())}
	labelTransitions =  {l: (f,w)for l, f, w in zip(labelTransitions.keys(), [len(words) for words in labelTransitions.values()], labelTransitions.values())}
	wordTransitions = {w: (f, l) for w, f, l in zip(wordTransitions.keys(), [len(labels) for labels in wordTransitions.values()], wordTransitions.values())}

	return labels, labelTransitions, wordTransitions, labelWordTransitions

def computeSecondOrderTransitions(labels, LabelList):
	'''
	Computes the transition matrix to the 2nd order
	params: 
		labels (list) => list of all the labels in the dataset in order
		labelList (nested list) => list of all the annotated labels in the dataset
	'''
	labels += ['START', 'STOP']
	k = len(labels)
	transitionMatrix = np.zeros((k ,k ,k))

	for labelSet in LabelList:

		# add start and stop into labels
		labelSet = ['START'] + labelSet + ['STOP']

		# init vars for looping
		prevLabel = 'START'
		secondPrevLabel = None

		# looping through each word and label
		for label in labelSet:

			if prevLabel and secondPrevLabel:
				# get the indexes of each label
				n = labels.index(secondPrevLabel)
				m = labels.index(prevLabel)
				o = labels.index(label)

				# add to transition matrix
				transitionMatrix[n][m][o] += 1

			secondPrevLabel = prevLabel
			prevLabel = label

	return transitionMatrix