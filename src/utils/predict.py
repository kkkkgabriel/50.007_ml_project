import numpy as np
from math import log
import copy

def simplePredict(text, emissionProbs, wordList, labels):
	'''
	Predicts labels for a list of words based on the max emission probability
	params:
		text (list) => list of words to predict labels on 
		emissionProbs (np.2darray) => matrix of emission probablities
		wordList (list) => list of words that exist in training set. It's order corresponds to the columns of emissionProbs.
		labels (dict) => dictionary of labels, storing its counts and word emissions. It's keys' order corresponds to the rows of emissionProbs.
	'''
	# init vars
	prediction = []
	labelList = list(labels.keys())
	
	# loop through each word
	for word in text:
		# try to get the index of the word in wordList
		# if word is not in wordList, set the wordIdx to access the last column in the emissionProbs
		try:
			wordIdx = wordList.index(word)
		except:
			wordIdx = -1

		# get the emission values and the max value for that word
		emissionValues = emissionProbs[:, wordIdx]
		maxEmissionValue = max(emissionValues)
		
		# get the label which the max value corresponds to
		labelIdx = np.where(emissionValues == maxEmissionValue)[0][0]
		label = labelList[labelIdx]
		
		# append label to prediction
		prediction.append(label)
	return prediction

def viterbi(alp, bet, wordList, labels, text):
	'''
	Predicts labels for a list of words based on the max emission probability
	params:
		alp (np.2darray) => matrix of transition probabilities
		bet (np.2darray) => matrix of emission probablities
		wordList (list) => list of words that exist in training set. It's order corresponds to the columns of emissionProbs.
		labels (dict) => dictionary of labels, storing its counts and word emissions. It's keys' order corresponds to the rows of emissionProbs.
		text (list) => list of words to predict labels on 
	'''
	# get list of labels including start and stop at the final 2 positions
	labels = list(labels.keys()) + ['START', 'STOP']

	# convert all text to their indexes in wordlist
	# if word does not exist in training set, set index to len(wordList)-3 to access UNK token
	# text = [wordList.index(word) if word in wordList else len(wordList)-3 for word in text]
	text = [wordList.index(word) if word in wordList else len(wordList)-1 for word in text]

	# state = # of states
	noOfStates = alp.shape[0]

	# sequenceLength = # of labels to be generated
	sequenceLength = len(text)

	# piTable inits as n by m matrix: to store the max value
	#     n rows: # of labels to be generated + 2 (to include start and stop?)
	#     m cols: # number of states
	piTable = np.ones((sequenceLength+2, noOfStates))
	piTable = piTable * (-np.inf)

	# maxYTable inits as n by m matrix: to store the location of the parent that gave the max value at each node in the piTable
	#     n: # of labels to be generated + 2 (to include start and stop?)
	#     m: # number of states 
	maxYTable = np.zeros((sequenceLength+2, noOfStates))

	# backwardsStateSequence starts with the stop state
	backwardsStateSequence = [noOfStates-1]

	# predetermine indexes of start and stop labels
	indexOfStart = noOfStates - 2
	indexOfStop = noOfStates - 1

	# first label will be 'start' of 100% certainty
	piTable[0][indexOfStart] = 1

	# for each position in sequence generation, find out the pi values for each possible transition (u -> v) and emission (v -> word)
	for row in range(1, sequenceLength+1):

		# loop through all possible v states 
		for v in range(noOfStates):
			maxPiValue = -np.inf 	  # to store highest probability
			maxPiIndex = 0    # to store index of state with highest probability

			# calculating probabilty of word emission from v -> word 
			word = text[row-1]
			emissionProbability = bet[v][word]

			# loop through all possible previous u states
			for u in range(noOfStates):
				transitionProbability = alp[u][v]  # calculating probabilty of state transition from u->v
				parentPi = piTable[row-1][u] 	   # getting pi value of previous state

				# calculate the pi value for this u -> v -> word possibility
				try:
					pi = log(emissionProbability) + log(transitionProbability) + parentPi

				except:
					pi = -np.inf

				# check if pi value if largest for this position
				if pi > maxPiValue:
					maxPiValue = pi
					maxPiIndex = u
					maxYTable[row][v] = maxPiIndex
					piTable[row][v] = maxPiValue

	# for the last state, calculation is different from the previous states: as there is no need to include beta
	maxPiValue = -np.inf   # to store highest probability
	maxPiIndex = 0    # to store index of state with highest probability

	for u in range(noOfStates):
		transitionProbability = alp[u][indexOfStop] # gets the transition probability from each state u -> stop
		parentPi = piTable[sequenceLength][u] # getting pi value of previous state

		# calculate the pi value for this u -> stop
		try: 
			finalPi = parentPi+log(transitionProbability)
		except:
			finalPi = -np.inf
		
		if finalPi > maxPiValue:
			maxPiValue = finalPi # get the greatest pi of the final transition
			maxPiIndex = u # get index of the state of the final transition
	backwardsStateSequence.append(maxPiIndex) # add the state to the backwards state seq
	# backtrack to pick out most probable states 
	for k in range(sequenceLength):
		idx = k+1
		row = sequenceLength-k
		parent = maxYTable[row][int(backwardsStateSequence[idx])]
		backwardsStateSequence.append(parent)

	# # flip the backwardsStateSequence and convert each state to its str value, assign that to forward seq
	forwardSequence = [labels[int(i)] for i in reversed(backwardsStateSequence)]

	# remove start and stop labels
	del forwardSequence[0]
	del forwardSequence[len(forwardSequence)-1]

	return forwardSequence

def k3BestViterbi(alp, bet, wordList, labels, text):
	# get list of labels including start and stop at the final 2 positions
	labels = list(labels.keys()) + ['START', 'STOP']

	# convert all text to their indexes in wordlist
	# if word does not exist in training set, set index to len(wordList)-3 to access UNK token
	text = [wordList.index(word) if word in wordList else len(wordList)-3 for word in text]

	# states = # of states
	noOfStates = alp.shape[0]

	# seqlen = # of labels to be generated
	sequenceLength = len(text)

	# predetermine indexes of start and stop labels
	indexOfStart = noOfStates - 2
	indexOfStop = noOfStates - 1

	# piTable inits as n by m by o matrix: to store the max value
	#     n rows: # of labels to be generated + 2 (to include start and stop?)
	#     m cols: # number of states
	#	  o depth: # k best (stores tuples)
	piTable = np.zeros((sequenceLength+2,noOfStates,3),dtype=[('str', np.object),('float',np.float)])

	# set start of be first label for all k best with 100% certainty
	piTable[0][noOfStates-2] = [('nil',1)]

	# to populate first state, which only have 1st best
	# loop through each state (indexOfStart is the index of the state)
	indexOfStart = noOfStates - 2
	for v in range(noOfStates):

		# calculate pi value for start -> v -> word 
		parentPi = piTable[0][indexOfStart][0][1]
		transitionProbability = alp[indexOfStart][v]
		emissionProbability = bet[v][text[0]]
		pi = parentPi * transitionProbability * emissionProbability

		# set the value on the pi table
		piTable[1][v][0] = (indexOfStart, pi)

	# for each position in sequence generation, find out the k best pi values for each possible transition (u -> v) and emission (v -> word)
	for row in range(2,sequenceLength+1):
		word = text[row-1]

		# loop through all possible j states
		for v in range(noOfStates):

			all_states = []
			for u in range(noOfStates):


				for k in piTable[row-1][u]:
					# find all pi values
					parentPi = k[1]
					transitionProbability = alp[u][v]
					emissionProbability = bet[v][text[row-1]]
					pi = parentPi * emissionProbability * transitionProbability

					# make transition string
					transitionString = str(k[0]) + ',' + str(u)

					# append to all states
					all_states.append((transitionString, pi))

			# sort state acc to pi value
			all_states.sort(key = lambda x: x[1], reverse=True)

			# get top pi tables and set to pi table
			piTable[row][v][0] = all_states[0]
			piTable[row][v][1] = all_states[1]
			piTable[row][v][2] = all_states[2]

	# init to store all final states 
	finalStates = []
	# for last row in piTable, loop through all pi values of 2nd last row for each state 
	# and multiply with transition probability to stop state
	for u in range(noOfStates):
		for k in piTable[sequenceLength][u]:
			# get pi value when transit to stop
			parentPi = k[1]
			transitionProbability = alp[u][indexOfStop]
			pi_add = parentPi * transitionProbability

			# get new string
			transitionString = str(k[0]) + ',' + str(u)

			# add to finalStates
			finalStates.append((transitionString, pi_add))

	# sort all final states
	finalStates.sort(key = lambda x: x[1],reverse=True)

	# get string of 3rd best state, concatenated with index of stop token
	kbest = str(finalStates[2][0]) + ',' + str(indexOfStop)

	# convert kbest into states
	kbest = [labels[int(idx)] for idx in kbest.split(',')]

	# remove the start and stop
	del kbest[0]
	del kbest[len(kbest)-1]

	return kbest
	
def secondOrderViterbi(alp, bet, wordList, labels, text):
	'''
	Predicts labels for a list of words based on the second order viterbi algorithm
	params:
		alp (np.3darray) => matrix of transition probabilities
		bet (np.2darray) => matrix of emission probablities
		wordList (list) => list of words that exist in training set. It's order corresponds to the columns of emissionProbs.
		labels (dict) => dictionary of labels, storing its counts and word emissions. It's keys' order corresponds to the rows of emissionProbs.
		text (list) => list of text to predict on 
	'''
	# get list of labels including start and stop at the final 2 positions
	labels = list(labels.keys()) + ['START', 'STOP']

	# convert all text to their indexes in wordlist
	# if word does not exist in training set, set index to len(wordList)-1 to access UNK token
	text = [wordList.index(word) if word in wordList else len(wordList)-1 for word in text]

	# state = # of states
	noOfStates = alp.shape[0]

	# sequenceLength = # of labels to be generated
	sequenceLength = len(text)

	# piTable inits as n by m matrix: to store the max value
	#     n rows: # of labels to be generated + 3 (to include 2 start and stop?)
	#     m cols: # number of states
	piTable = np.zeros((sequenceLength+3, noOfStates), dtype=[('str', np.object),('float',np.float)])

	# predetermine indexes of start and stop labels
	indexOfStart = noOfStates - 2
	indexOfStop = noOfStates - 1

	# init first 2 rows to be all zeros, only 'START' -> 'START' to have 100% certainyu
	piTable[0] = [(str(indexOfStop) + ',' + str(indexOfStop), 0)]
	piTable[1] = [(str(indexOfStop) + ',' + str(indexOfStop), 0)]
	piTable[0][indexOfStart] = (str(indexOfStart), 1)
	piTable[1][indexOfStart] = (str(indexOfStart) + ',' + str(indexOfStart), 1)

	# for each position in sequence generation, find out the pi values for each possible transition (u -> v) and emission (v -> word)
	for row in range(2, sequenceLength+2):
		# loop through all possible v states 
		for v in range(noOfStates):
			maxPiValue = -1    # to store highest probability
			maxPiSequence = '' # to store the sequence with the highest probability
			
			# calculating probabilty of word emission from v -> word
			word = text[row-2]
			emissionProbability = bet[v][word]

			# loop through all possible previous u states
			for u in range(noOfStates):
				sequence = piTable[row-1][u][0]  # getting the sequence from the tuple
				t = int(sequence.split(',')[-2]) # get t, which will be the second last number in the sequence
				
				transitionProbability = alp[t][u][v]  # calculating probabilty of state transition from t->u->v
				parentPi = piTable[row-1][u][1]       # getting pi value of previous state

				# calculate the pi value for this t -> u -> v -> word possibility
				pi = emissionProbability * transitionProbability * parentPi

				# check if pi value if largest for this position
				if pi > maxPiValue:
					maxPiValue = pi
					maxPiSequence = sequence + ',' + str(v)
			piTable[row][v] = (maxPiSequence, maxPiValue)

	# for the last state, calculation is different from the previous states: as there is no need to include beta
	maxPiValue = -1    # to store highest probability
	maxPiIndex = 0    # to store index of state with highest probability
	
	for u in range(noOfStates):
		sequence = piTable[row][u][0] # getting the sequence from the tuple
		t = int(sequence.split(',')[-2]) # get t, which will be the second last number in the sequence
		
		transitionProbability = alp[t][u][indexOfStop] # gets the transition probability from each state t -> u -> stop
		parentPi = piTable[row][u][1] # getting pi value of previous state

		# calculate the pi value for this t -> u -> stop
		finalPi = parentPi * transitionProbability

		if finalPi > maxPiValue:
			maxPiValue = finalPi # get the greatest pi of the final transition
			maxPiIndex = u # get index of the state of the final transition

	# retrieve sequence once maxPiIndex is found from the last state and add index of STOP
	stateSequence = piTable[row][maxPiIndex][0] + ',' + str(indexOfStop)
	stateSequence = stateSequence.split(',')

	# convert to the states 
	stateSequence = [ labels[int(state)] for state in stateSequence]
	

	# remove start and stop tokens
	del stateSequence[0]
	del stateSequence[0]
	del stateSequence[len(stateSequence)-1]
	
	return stateSequence

def kBestViterbi(alp, bet, wordList, labels, text, kbest=3, reverse=False):
	'''
	Predicts the labels for a text, accounting for the predictions generated by the forward and reverse viterbi
	params
		alp (np.2darray) => matrix of transition probabilities
		betaWithUNK (np.2darray) => matrix of emission probablities
		wordList (list) => list of words that exist in training set. It's order corresponds to the columns of emissionProbs.
		labels (dict) => dictionary of labels, storing its counts and word emissions. It's keys' order corresponds to the rows of emissionProbs.
		text (list) => list of text to predict on 
		kbest, default = 3 (int) => best k results to look through with forward and reverse viterbi
		reverse, default = False (bool) => set this to true if this function is used to predict on reverse text
	return:
		convertedArray (list) => nested list of k best predictions
		finalScoresarray (list) => list of all the scores corresponding to the k best predictions
	'''
	# get list of labels including start and stop at the final 2 positions
	labels = list(labels.keys()) + ['START', 'STOP']

	# convert all text to their indexes in wordlist
	# if word does not exist in training set, set index to len(wordList)-1 to access UNK token
	if reverse:
		reverseText = copy.deepcopy(text)
		reverseText.reverse()
		text = reverseText
	text = [wordList.index(word) if word in wordList else len(wordList)-1 for word in text]

	# states = # of states
	noOfStates = alp.shape[0]

	# seqlen = # of labels to be generated
	sequenceLength = len(text)

	# predetermine indexes of start and stop labels
	indexOfStart = noOfStates - 2
	indexOfStop = noOfStates - 1

	# piTable inits as n by m by o matrix: to store the max value
	#     n rows: # of labels to be generated + 2 (to include start and stop?)
	#     m cols: # number of states
	#	  o depth: # k best (stores tuples)
	piTable = np.ones((sequenceLength+2, noOfStates, kbest), dtype=[('str', np.object),('float',np.float)])
	for i in range(sequenceLength+2):
		for j in range(noOfStates):
			for k in range(kbest):
				piTable[i][j][k] = (str(indexOfStart), -np.inf)

	# set start of be first label for all k best with 100% certainty
	piTable[0][noOfStates-2] = [('nil',1)]

	# to populate first state, which only have 1st best
	# loop through each state (indexOfStart is the index of the state)
	indexOfStart = noOfStates - 2
	for v in range(noOfStates):

		# calculate pi value for start -> v -> word 
		parentPi = piTable[0][indexOfStart][0][1]
		transitionProbability = alp[indexOfStart][v]
		emissionProbability = bet[v][text[0]]

		try:
			pi = parentPi + log(transitionProbability) + log(emissionProbability)
		except:
			pi = -np.inf

		# set the value on the pi table
		piTable[1][v][0] = (indexOfStart, pi)

	# for each position in sequence generation, find out the k best pi values for each possible transition (u -> v) and emission (v -> word)
	for row in range(2,sequenceLength+1):
		word = text[row-1]

		# loop through all possible j states
		for v in range(noOfStates):

			all_states = []
			for u in range(noOfStates):


				for k in piTable[row-1][u]:
					# find all pi values
					parentPi = k[1]
					transitionProbability = alp[u][v]
					emissionProbability = bet[v][text[row-1]]
					try:
						pi = parentPi + log(transitionProbability) + log(emissionProbability)
					except:
						pi = -np.inf

					# make transition string
					transitionString = str(k[0]) + ',' + str(u)

					# append to all states
					all_states.append((transitionString, pi))

			# sort state acc to pi value
			all_states.sort(key = lambda x: x[1], reverse=True)

			# get top pi tables and set to pi table
			for i2 in range(kbest):
				piTable[row][v][i2] = all_states[i2]

	# init to store all final states 
	finalStates = []
	finalStatesarray = []
	convertedarray = []
	finalScoresarray = []
	
	# for last row in piTable, loop through all pi values of 2nd last row for each state 
	# and multiply with transition probability to stop state
	for u in range(noOfStates):
		for k in piTable[sequenceLength][u]:

			# get pi value when transit to stop
			parentPi = k[1]
			transitionProbability = alp[u][indexOfStop]

			# calculate the pi value for this u -> v -> word possibility
			try:
				pi = log(transitionProbability) + parentPi
			except:
				pi = -np.inf

			# get new string
			transitionString = str(k[0]) + ',' + str(u)

			# add to finalStates
			finalStates.append((transitionString, pi))

	# sort all final states
	finalStates.sort(key = lambda x: x[1], reverse=True)

	# get string of 3rd best state, concatenated with index of stop token
	# kbest = str(finalStates[k-1][0]) + ',' + str(indexOfStop)
	for f in range(kbest):
		finalStatesarray.append(finalStates[f][0])
		
	# convert kbest into states
	for seq in finalStatesarray:
		kbestseq = [labels[int(idx)] for idx in seq.split(',')]

		# remove the start state
		del kbestseq[0]
		
		
		if reverse:
			kbestseq.reverse()
		convertedarray.append(kbestseq)
	
	for f in range(kbest):
		finalScoresarray.append(finalStates[f][1])

	return convertedarray, finalScoresarray

def viterbitervi(
		alpha,
		reverseAlpha,
		betaWithUNK,
		wordList,
		labels,
		text,
		k = 5,
		maxK = 15,
		forwardBias = 1
	):
	'''
	Predicts the labels for a text, accounting for the predictions generated by the forward and reverse viterbi
	params
		alp (np.2darray) => matrix of transition probabilities
		betaWithUNK (np.2darray) => matrix of emission probablities
		wordList (list) => list of words that exist in training set. It's order corresponds to the columns of emissionProbs.
		labels (dict) => dictionary of labels, storing its counts and word emissions. It's keys' order corresponds to the rows of emissionProbs.
		text (list) => list of text to predict on 
		k, default = 5 (int) => best k results to look through with forward and reverse viterbi
		maxK, default = 15 (int) => maxK that k can get to
		forwardBias, default = 1 (int) => a factor to multiply the forward score with when computing the overall score
	returns:
		(list) => list predicted labels
	'''
	# init list to store common prediction
	# 	common predictions list to store list of tuples: (predictions, score)
	commonPredictions = []
	
	while True:
		# get forward predictions and scores
		forwardPreds, forwardScores =  kBestViterbi(alpha, betaWithUNK, wordList, labels, text, k)

		# get reverse predictions and scores
		reversePreds, reverseScores = kBestViterbi(reverseAlpha, betaWithUNK, wordList, labels, text, k, reverse=True)
		
		# get common predictions
		for forwardIdx, forwardPred in enumerate(forwardPreds):
			
			# if forward prediction is found reverse predictions, compute score and add (prediction, score) to common predictions
			if forwardPred in reversePreds:
				# get the scores for the prediction in forward and reverse
				forwardPredScore = forwardScores[forwardIdx]
				reversePredScore = reverseScores[reversePreds.index(forwardPred)]

				# calculate score and append to common predictions
				score = (forwardBias * forwardPredScore) + reversePredScore
				commonPredictions.append((forwardPred, score))
				
		# if common predictions are found, get the prediction with the highest score
		if len(commonPredictions) > 0:
			# sort the common predictions by the scores
			commonPredictions.sort(reverse=True, key=lambda x: x[1])

			# return the prediction with the highest score
			return commonPredictions[0][0]

		# if there are no common predictions, increase k count to get predictions again
		else:
			k += k
		
		# maxK is a limit that k can go to, once k exceeds maxK, return the best prediction from forward/backward, depending on forward bias
		if k > maxK:
			if forwardBias >= 1:
				return forwardPreds[0]
			else:
				return reversePreds[0]
		