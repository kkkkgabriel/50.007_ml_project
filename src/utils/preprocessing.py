import nltk
import pickle
nltk.download('averaged_perceptron_tagger')


def replaceNNPs(sentence):
	'''
	Replaces proper nouns in sentence with 'NNP'.
	For example:
		' All travellers to Singapore from Malaysia,
		Japan to serve COVID-19 stay-home notice at dedicated facilities.'
		... would be processed into ...
		' All travellers to NNP from NNP
		NNP to serve NNP stay-home notice at dedicated facilities.'
	'''
	# tag sentences and extract proper nouns
	taggedSentence = nltk.pos_tag(sentence.split())
	propernouns = [word for word, pos in taggedSentence if pos == 'NNP']

	# replace proper nouns in the sentence
	for word in propernouns:
		sentence = sentence.replace(word, 'NNP')
	return sentence

def fullPreprocessing(texts):
	'''
	preprocesses rawText all selected preprocessing methods
	param: text (list) => list of raw texts
	returns: (list) => list of preprocessed texts
	'''
	return [replaceNNPs(text) for text in texts]

def tokenize(texts, wordListLoc=None):
	'''
	Assigns a token to each unique word in the corpus. Returns a 
	list of tokens corresponding to each word in each sentence.
	params:
		texts (list) => list of raw texts.
		wordListLoc (string) [optional] => location to export the wordList to.
	returns:
		tokens (list) => nested list of tokens which corresponds to 
			the each word in each sentence
		wordList (list) => the corpus of the dataset in a list format. The tokens
			are the indexes of the word in the wordList.
	'''
	# init wordlist and tokens
	tokenList = []
	wordList = []

	# loop through text to populate word list
	for text in texts:
		# loop through each word in individual text
		for word in text.split(): 
			# only add word to wordlist if its not in yet
			if not word in wordList: wordList.append(word) 

	# loop through texts to get tokenlists
	for text in texts:
		# create empty list to store tokens for each individual text
		tokens = []

		for word in text.split():
			# get the index of the word in the wordlist and add to tokens
			tokens.append(wordList.index(word))

		# add the tokens to the tokenList
		tokenList.append(tokens)

	# if location for wordList is given, export the wordList as a pkl file
	if wordListLoc:
		with open(wordListLoc, 'wb') as f:
			pickle.dump(wordList, f)

	return tokenList, wordList
		


def main():
	sentence = '''
	All travellers to Singapore from Malaysia,
	Japan to serve COVID-19 stay-home notice at dedicated facilities.
	'''
	sentence = replaceNNPs(sentence)
	print(sentence)

if __name__ == '__main__':
	main()