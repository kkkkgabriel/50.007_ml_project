import numpy as np

def countMatrix(labelWordTransitions, labels, wordList):
	'''
	Sets up a matrix which counts the number of times a label emits a word
	params:
		labelWordTransitions (dict) => dictionary of label > word transitions with their counts as the value
		labels (dict) => dictionary of labels, storing its counts and word emissions.
		wordList (list) => list of words that exist in training set.
	return:
		labelToWordCount (2d np array) => array storing the number of times a label emits a word. The row headers are the labels, the col headers are the words.
	'''
	
	# extract unique labels from labels dictionary
	uniqLabels = list(labels.keys())

	# init n by m array to store the label to word count
	# n: number of labels (excluding START and STOP)
	# m: number of words in word list
	labelToWordCount = np.zeros((len(uniqLabels), len(wordList)))
	
	# loop through all label word transitions
	for transition in labelWordTransitions.items():
		
		# get the words for each transition
		splited = transition[0].split(' > ')
		count = transition[1]
		
		# find index of the label in UniqLabels
		if (splited[0] != 'START' and splited[0] != 'STOP' and splited[1] != 'STOP' and splited[1] != 'STOP'):
			
			# find the index of the word in wordList and the index of the label in uniqLabels
			wordIdx = wordList.index(splited[1])
			labelIdx = uniqLabels.index(splited[0])
			
			# add 1 to the count
			labelToWordCount[labelIdx][wordIdx] += count 
			
	return labelToWordCount

def emissionParameters(labelToWordCount, labels, wordList):
	'''
	calculates the emissions probability for every label to word
	params: 
		labelToWordCount (2d np array) => array storing the number of times a label emits a word. The row headers are the labels, the col headers are the words.
		labels (dict) => dictionary of labels, storing its counts and word emissions.
		wordList (list) => list of words that exist in training set.
	returns:
		emissionProbability (2d np array) => array storing the probability that a label emits a word. The row headers are the labels, the col headers are the words.
	'''

	# extract unique labels and the corresponding frequency from labels dictionary
	uniqLabels = list(labels.keys())
	uniqlabelsFreq = [item[0] for item in labels.values()]
	
	# init n by m array for to store emissionFreq
	# n: number of label (including START and STOP)
	# m: number of words in wordList
	emissionProbability = np.zeros((len(uniqLabels), len(wordList)))
	
	# loop through all the unique labels
	for i in range(len(uniqLabels)):
		# get the corresponding frequency
		labelFreq = uniqlabelsFreq[i]
		
		# loop through word list
		for j in range(len(wordList)):
			
			# get the count from label to word
			count = labelToWordCount[i][j]
			
			# set the freq to be count/freq
			emissionProbability[i][j] = count / labelFreq
			
	return emissionProbability

def accountForUNK(labelToWordCount, labels, k=0.5):
	'''
	calculates the emissions probability for every label to word
	params: 
		labelToWordCount (2d np array) => array storing the number of times a label emits a word. The row headers are the labels, the col headers are the words.
		labels (dict) => dictionary of labels, storing its counts and word emissions.
	returns:
		emissionProbabilityUNK (2d np array) => array storing the probability that a label emits a word, accounting for unknown words. The row headers are the labels, the col headers are the words.
	'''

	# extract unique labels and the corresponding frequency from labels dictionary
	uniqLabels = list(labels.keys())
	uniqlabelsFreq = [item[0] for item in labels.values()]

	# initialise a new array
	size = labelToWordCount.shape
	emissionProbabilityUNK = np.zeros((size[0], size[1]+1))
	
	# loop through all the unique labels
	for v in range(len(uniqLabels)):
		# add k to the each frequency value
		labelFreq = uniqlabelsFreq[v] + k
		
		# loop through emission counts for each destination label
		for word in range(size[1] + 1):
			# the count for each label for the #UNK token will be the last column of the array
			if (word == size[1]):
				count = k 
			else:
				count = labelToWordCount[v][word]
				
			# set the value of the custom emissionFreq
			emissionProbabilityUNK[v][word] = count / labelFreq
			
	return emissionProbabilityUNK

def calculateAlpha(labels, labelTransitions, noOfData):
	'''
	calculates the probability that a label transits to another
	params: 
		noOfData (int) => number of documents in training set
		labelTransitions (dict) => dict storing the number of times a label transits to another, as well as the words that it emits.
		labels (dict) => dictionary of labels, storing its counts and word emissions.
	returns:
		labelList (list) => row and columns headers for alpha
		alpha (2d np array) => array storing probability that each label transits to another. Row and columns headers are labelList.
	'''
	# get the labels in list format and add 'START' and 'STOP'
	labelList = list(labels.keys()) + ['START', 'STOP']
	
	# init n by n array of zeros (n for the number of states)
	# value in the 2d array to be read as probability of transition from row to col header
	alpha = np.zeros((len(labelList),len(labelList)))

	# loop through the states(i) for u
	for i, u in enumerate(labelList):
		
		# loop through the states(j) for v
		for j, v in enumerate(labelList):
			
			# numerator = frequency(u->v)
			try:
				numerator = labelTransitions[u + ' > ' + v][0]
			except:
				numerator = 0 # if transition does not exist in labelTransitions, means that there is no such transition
			
			# denominator = frequency(u)
			if u == 'START' or u == 'STOP':
				denominator = noOfData # start and stop are not listed in labels, but their count would be equal to the no of data
			else:
				denominator = labels[u][0]
			
			# calculate frequency(u->v)/frequency(u) and place value in alpha array
			alpha[i][j] = numerator/denominator
			
	return alpha, labelList

def calculateReverseAlpha(labels, labelTransitions, noOfData):
	'''
	generate the reverse transition probabilities using the label transitions
	params:
		noOfData (int) => number of documents in training set
		labelTransitions (dict) => dict storing the number of times a label transits to another, as well as the words that it emits.
		labels (dict) => dictionary of labels, storing its counts and word emissions.
	returns:
		(reverseAlpha, labelList) (tuple) =>
			labelList (list) => row and columns headers for alpha
			alpha (2d np array) => array storing probability that each label transits to another. Row and columns headers are labelList.
	'''

	# dicionary to store all the reverse label transitions
	reverseLabelTransitions = {}

	for transition, value in labelTransitions.items():

		# split the transition to get the labels
		label1, label2 = transition.split(' > ')
		
		# swap start and stop if any of the labels are start or stop
		if label1 == 'STOP':
			label1 = 'START'
		elif label1 == 'START':
			label1 = 'STOP'
		if label2 == 'STOP':
			label2 = 'START'
		elif label2 == 'START':
			label2 = 'STOP'
			
		# create new transition string
		newTransition = label2 + ' > ' + label1

		# the count of the transition would be equal to the count of the original
		count = value[0]

		# add the new transition to the dictionary
		reverseLabelTransitions[newTransition] = (count, [])
	   
	# use the calculate alpha function to calculate the reverse alpha
	return calculateAlpha(labels, reverseLabelTransitions, noOfData)

def calculateBeta(labelWordTransitions, labels, wordList, includeStartAndStop=True):
	'''
	function to calculate beta
	params:
		labelWordTransitions (dict) => dictionary of label > word transitions with their counts as the value
		labels (dict) => dictionary of labels, storing its counts and word emissions.
		wordList (list) => list of words that exist in training set.
		includeStartAndStop (bool) => if true, 2 rows of 0's will be added to be emissions probabilities for start and stop labels
	returns:
		beta (2d np array) => array storing the probability that a label emits a word. The row headers are the labels, the col headers are the words.
		indexHeaders (list) => index for df presentation
		wordList (list) => columns for df presentation
	'''
	labelToWordCount = countMatrix(labelWordTransitions, labels, wordList)
	beta = emissionParameters(labelToWordCount, labels, wordList)
	indexHeaders = list(labels.keys())

	if includeStartAndStop:
		beta = np.vstack((beta, np.zeros((2, beta.shape[1]))))
		indexHeaders += ['START','STOP']
		
	return beta, indexHeaders, wordList

def calculateBetaWithUNK(labelWordTransitions, labels, wordList, includeStartAndStop=True, k=0.5):
	'''
	function to calculate beta accounting for unknown words
	params:
		labelWordTransitions (dict) => dictionary of label > word transitions with their counts as the value
		labels (dict) => dictionary of labels, storing its counts and word emissions.
		wordList (list) => list of words that exist in training set.
	returns:
		betaWithUNK (2d np array) => array storing the probability that a label emits a word. The row headers are the labels, the col headers are the words.
	'''
	labelToWordCount = countMatrix(labelWordTransitions, labels, wordList)
	betaWithUNK = accountForUNK(labelToWordCount, labels, k=k)
	indexHeaders = list(labels.keys())
	wordList += ['UNK']
	if includeStartAndStop:
		betaWithUNK = np.vstack((betaWithUNK, np.zeros((2, betaWithUNK.shape[1]))))
		indexHeaders += ['START','STOP']
	return betaWithUNK, indexHeaders, wordList



def calculateSecondOrderAlpha(labels, labelTransitions, noOfData):
	'''
	calculates the probability that a label transits to another
	params: 
		noOfData (int) => number of documents in training set
		labelTransitions (dict) => dict storing the number of times a label transits to another, as well as the words that it emits.
		labels (dict) => dictionary of labels, storing its counts and word emissions.
	returns:
		labelList (list) => row and columns headers for alpha
		alpha (2d np array) => array storing probability that each label transits to another. Row and columns headers are labelList.
	'''
	# get the labels in list format and add 'START' and 'STOP'
	labelList = list(labels.keys()) + ['START', 'STOP']

	# init n by n by n array of zeros (n for the number of states)
	# value in the 2d array to be read as probability of transition from row to col header
	alpha = np.zeros((len(labelList),len(labelList), len(labelList)))

	#loop through the states(k) for t
	for t, labelt in enumerate(labelList):

		# loop through the states(i) for u
		for u, labelu in enumerate(labelList):

			# loop through the states(j) for v
			for v, labelv in enumerate(labelList):

				# numerator = frequency(w->u->v)
				try:
					numerator = labelTransitions[t][u][v]
				except:
					numerator = 0 # if transition does not exist in labelTransitions, means that there is no such transition

				# denominator = frequency(t)
				if labelt == 'START' or labelt == 'STOP':
					denominator = noOfData # start and stop are not listed in labels, but their count would be equal to the no of data
				else:
					denominator = labels[labelt][0]

				# calculate frequency(u->v)/frequency(u) and place value in alpha array
				alpha[t][u][v] = numerator/denominator

	return alpha